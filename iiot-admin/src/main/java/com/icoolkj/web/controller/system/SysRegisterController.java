package com.icoolkj.web.controller.system;

import com.icoolkj.common.core.controller.BaseController;
import com.icoolkj.common.core.domain.AjaxResult;
import com.icoolkj.common.core.domain.entity.SysRegister;
import com.icoolkj.common.core.domain.model.CompRegisterBody;
import com.icoolkj.common.core.domain.model.RegisterBody;
import com.icoolkj.common.utils.StringUtils;
import com.icoolkj.framework.web.service.ValidateCaptchaService;
import com.icoolkj.system.service.ISysConfigService;
import com.icoolkj.system.service.ISysRegisterService;
import com.icoolkj.web.biz.system.SysRegisterBiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 注册验证
 *
 * @author icoolkj
 */
@RestController
public class SysRegisterController extends BaseController
{
    @Autowired
    private ISysRegisterService sysRegisterService;

    @Autowired
    private ISysConfigService configService;

    @Autowired
    private ValidateCaptchaService validateCaptchaService;

    @Autowired
    private SysRegisterBiz sysRegisterBiz;



    @PostMapping("/register")
    public AjaxResult register(@RequestBody RegisterBody user)
    {
        if (!("true".equals(configService.selectConfigByKey("sys.account.registerUser"))))
        {
            return error("当前系统没有开启注册功能！");
        }
        //String msg = sysRegisterBiz.register(user);
        //return StringUtils.isEmpty(msg) ? success() : error(msg);
        return error();
    }

    @PostMapping("/compRegister")
    public AjaxResult compRegister(@Validated @RequestBody CompRegisterBody compRegisterBody)
    {
        if (!("true".equals(configService.selectConfigByKey("sys.account.compRegister"))))
        {
            return error("当前系统没有开启企业注册功能！");
        }
        // 验证码开关
        boolean captchaEnabled = configService.selectCaptchaEnabled();
        if (captchaEnabled) {
            validateCaptchaService.validateCaptcha(compRegisterBody.getCode(), compRegisterBody.getUuid());
        }
        // 注册账号的唯一性
        SysRegister sysRegisterNew = sysRegisterService.checkRegisterAccountUnique(compRegisterBody.getRegisterType(), compRegisterBody.getRegisterAccount(), compRegisterBody.getRegisterNickName());
        if (StringUtils.isNotNull(sysRegisterNew)){
            if ("1".equals(sysRegisterNew.getRegisterStatus())) { //账号注册状态（1代表申请中|2代表审核通过|3代表审核不通过）
                return error("账号'"+ compRegisterBody.getRegisterAccount() + "'注册失败，当前账号正处于审核中。");
            }
            if ("2".equals(sysRegisterNew.getRegisterStatus())) { //账号注册状态（1代表申请中|2代表审核通过|3代表审核不通过）
                return error("账号'"+ compRegisterBody.getRegisterAccount() + "'注册失败，当前账号已经存在。");
            }
            return error("注册失败,请联系系统管理人员！");
        }
        String msg = sysRegisterBiz.compRegister(compRegisterBody);
        return StringUtils.isEmpty(msg) ? success() : error(msg);
    }
}
