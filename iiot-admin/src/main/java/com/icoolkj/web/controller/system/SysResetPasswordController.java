package com.icoolkj.web.controller.system;

import com.icoolkj.common.constant.SysConstants;
import com.icoolkj.common.core.controller.BaseController;
import com.icoolkj.common.core.domain.AjaxResult;
import com.icoolkj.common.core.domain.entity.SysResetPassword;
import com.icoolkj.common.core.domain.entity.SysUser;
import com.icoolkj.common.core.domain.model.ResetPasswordBody;
import com.icoolkj.common.utils.DataMaskingUtils;
import com.icoolkj.common.utils.StringUtils;
import com.icoolkj.framework.web.service.ValidateCaptchaService;
import com.icoolkj.system.service.ISysConfigService;
import com.icoolkj.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * 重置账号密码
 *
 * @author icoolkj
 */
@RestController
@RequestMapping("/resetPassword")
public class SysResetPasswordController extends BaseController
{
    @Autowired
    private ISysConfigService configService;

    @Autowired
    private ValidateCaptchaService validateCaptchaService;

    @Autowired
    private ISysUserService userService;

    @PostMapping("/getUserInfo")
    public AjaxResult getUserInfo(@Validated @RequestBody ResetPasswordBody resetPasswordBody)
    {
        // 验证码开关
        boolean captchaEnabled = configService.selectCaptchaEnabled();
        if (captchaEnabled) {
            validateCaptchaService.validateCaptcha(resetPasswordBody.getCode(), resetPasswordBody.getUuid());
        }
        String userType = resetPasswordBody.getUserType();
        String userName = resetPasswordBody.getUserName();
        if ("COMP".equals(userType)){
            userName = SysConstants.PREFIX_COMP_ACCOUNT + userName;
        } else if ("ZHFWJG".equals(userType)){
            userName = SysConstants.PREFIX_COMP_ZHFWJG + userName;
        } else if ("FWS".equals(userType)){
            userName = SysConstants.PREFIX_COMP_FWS + userName;
        } else {
            userName = SysConstants.PREFIX_COMP_SYSTEM + userName;
        }
        SysUser sysUser = userService.selectUserByUserName(userName);
        if (StringUtils.isNull(sysUser) || StringUtils.isEmpty(sysUser.getUserName())){
            return error("用户账号或验证码错误");
        }
        HashMap map = new HashMap();
        map.put("userName", resetPasswordBody.getUserName());
        map.put("userEmail", DataMaskingUtils.email(sysUser.getEmail()));
        map.put("userPhone", DataMaskingUtils.mobilePhone(sysUser.getPhonenumber()));
        return success(map);
    }

    @PostMapping("/verifyUserInfo")
    public AjaxResult verifyUserInfo(@RequestBody SysResetPassword sysResetPassword)
    {
        String userType = sysResetPassword.getUserType();
        String userName = sysResetPassword.getUserName();
        if ("COMP".equals(userType)){
            userName = SysConstants.PREFIX_COMP_ACCOUNT + userName;
        } else if ("ZHFWJG".equals(userType)){
            userName = SysConstants.PREFIX_COMP_ZHFWJG + userName;
        } else if ("FWS".equals(userType)){
            userName = SysConstants.PREFIX_COMP_FWS + userName;
        } else {
            userName = SysConstants.PREFIX_COMP_SYSTEM + userName;
        }
        SysUser sysUser = userService.selectUserByUserName(userName);
        if (StringUtils.isNull(sysUser) || StringUtils.isEmpty(sysUser.getUserName())){
            return error("用户身份验证失败，请重新操作");
        }
        if ("1".equals(sysResetPassword.getVerifyMode())){

        } else if ("2".equals(sysResetPassword.getVerifyMode())){

        } else {
            return error("用户身份验证失败，请重新操作");
        }
        return success();
    }


}
