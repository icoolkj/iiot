package com.icoolkj.company.service.impl;

import com.icoolkj.common.utils.DateUtils;
import com.icoolkj.common.utils.SecurityUtils;
import com.icoolkj.common.utils.uuid.IdWorker;
import com.icoolkj.company.domain.DcCompBasic;
import com.icoolkj.company.mapper.DcCompBasicMapper;
import com.icoolkj.company.service.IDcCompBasicService;
import com.icoolkj.file.service.impl.FileUploadManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 企业基本信息Service业务层处理
 *
 * @author icoolkj
 * @date 2023-03-10
 */
@Service
public class DcCompBasicServiceImpl implements IDcCompBasicService
{
    @Autowired
    private DcCompBasicMapper dcCompBasicMapper;

    @Autowired
    private FileUploadManageService fileUploadManageService;

    /**
     * 查询企业基本信息
     *
     * @param compBasicId 企业基本信息主键
     * @return 企业基本信息
     */
    @Override
    public DcCompBasic selectDcCompBasicByCompBasicId(String compBasicId)
    {
        return dcCompBasicMapper.selectDcCompBasicByCompBasicId(compBasicId);
    }

    /**
     * 查询企业基本信息列表
     *
     * @param dcCompBasic 企业基本信息
     * @return 企业基本信息
     */
    @Override
    public List<DcCompBasic> selectDcCompBasicList(DcCompBasic dcCompBasic)
    {
        return dcCompBasicMapper.selectDcCompBasicList(dcCompBasic);
    }

    /**
     * 新增企业基本信息
     *
     * @param dcCompBasic 企业基本信息
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertDcCompBasic(DcCompBasic dcCompBasic)
    {
        dcCompBasic.setCompBasicId(IdWorker.nextId().toString());
        dcCompBasic.setCreateBy(SecurityUtils.getLoginUser().getUser().getUserName());
        dcCompBasic.setCreateTime(DateUtils.getNowDate());
        return dcCompBasicMapper.insertDcCompBasic(dcCompBasic);
    }

    /**
     * 修改企业基本信息
     *
     * @param dcCompBasic 企业基本信息
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateDcCompBasic(DcCompBasic dcCompBasic)
    {
        dcCompBasic.setUpdateBy(SecurityUtils.getLoginUser().getUser().getUserName());
        dcCompBasic.setUpdateTime(DateUtils.getNowDate());
        fileUploadManageService.dealFileInfo(dcCompBasic.getCompBasicId(), dcCompBasic.getFileIds());
        return dcCompBasicMapper.updateDcCompBasic(dcCompBasic);
    }

    /**
     * 根据统一社会信用代码获取信息
     *
     * @param dcCompBasic
     * @return 结果
     */
    @Override
    public DcCompBasic getDcCompBasicByCreditCode(DcCompBasic dcCompBasic)
    {
        return dcCompBasicMapper.getDcCompBasicByCreditCode(dcCompBasic.getCompCreditCode());
    }

    /**
     * 创建企业基本信息
     *
     * @param dcCompBasic 企业基本信息
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public DcCompBasic createDcCompBasic(DcCompBasic dcCompBasic)
    {
        dcCompBasicMapper.insertDcCompBasic(dcCompBasic);
        return dcCompBasic;
    }

}
