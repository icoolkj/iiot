package com.icoolkj.system.mapper;

import com.icoolkj.common.core.domain.entity.SysResetPassword;

import java.util.List;

/**
 * 密码重置信息Mapper接口
 *
 * @author icoolkj
 */
public interface SysResetPasswordMapper
{
    /**
     * 查询密码重置信息
     *
     * @param resetPwdId 密码重置信息主键
     * @return 密码重置信息
     */
    public SysResetPassword selectSysResetPasswordByResetPwdId(String resetPwdId);

    /**
     * 查询密码重置信息列表
     *
     * @param sysResetPassword 密码重置信息
     * @return 密码重置信息集合
     */
    public List<SysResetPassword> selectSysResetPasswordList(SysResetPassword sysResetPassword);

    /**
     * 新增密码重置信息
     *
     * @param sysResetPassword 密码重置信息
     * @return 结果
     */
    public int insertSysResetPassword(SysResetPassword sysResetPassword);

    /**
     * 修改密码重置信息
     *
     * @param sysResetPassword 密码重置信息
     * @return 结果
     */
    public int updateSysResetPassword(SysResetPassword sysResetPassword);

    /**
     * 删除密码重置信息
     *
     * @param resetPwdId 密码重置信息主键
     * @return 结果
     */
    public int deleteSysResetPasswordByResetPwdId(String resetPwdId);

    /**
     * 批量删除密码重置信息
     *
     * @param resetPwdIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSysResetPasswordByResetPwdIds(String[] resetPwdIds);
}
