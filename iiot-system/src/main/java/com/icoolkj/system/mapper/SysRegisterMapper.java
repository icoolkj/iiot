package com.icoolkj.system.mapper;

import com.icoolkj.common.core.domain.entity.SysRegister;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 注册信息Mapper接口
 *
 * @author icoolkj
 */
public interface SysRegisterMapper
{
    /**
     * 查询注册信息
     *
     * @param registerId 注册信息主键
     * @return 注册信息
     */
    public SysRegister selectSysRegisterByRegisterId(String registerId);

    /**
     * 查询注册信息列表
     *
     * @param sysRegister 注册信息
     * @return 注册信息集合
     */
    public List<SysRegister> selectSysRegisterList(SysRegister sysRegister);

    /**
     * 新增注册信息
     *
     * @param sysRegister 注册信息
     * @return 结果
     */
    public int insertSysRegister(SysRegister sysRegister);

    /**
     * 修改注册信息
     *
     * @param sysRegister 注册信息
     * @return 结果
     */
    public int updateSysRegister(SysRegister sysRegister);


    public SysRegister checkRegisterAccountUnique(@Param("registerType") String registerType, @Param("registerAccount") String registerAccount, @Param("registerNickName") String registerNickName);
}
