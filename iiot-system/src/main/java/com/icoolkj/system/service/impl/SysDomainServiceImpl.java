package com.icoolkj.system.service.impl;

import com.icoolkj.common.constant.SysConstants;
import com.icoolkj.common.constant.UserConstants;
import com.icoolkj.common.core.domain.entity.SysDomain;
import com.icoolkj.common.utils.DateUtils;
import com.icoolkj.common.utils.SecurityUtils;
import com.icoolkj.common.utils.uuid.IdWorker;
import com.icoolkj.company.domain.DcCompBasic;
import com.icoolkj.system.mapper.SysDomainMapper;
import com.icoolkj.system.service.ISysDomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.List;

/**
 * 系统组织账户Service业务层处理
 *
 * @author icoolkj
 * @date 2023-03-08
 */
@Service
public class SysDomainServiceImpl implements ISysDomainService
{
    @Autowired
    private SysDomainMapper sysDomainMapper;

    /**
     * 查询系统组织账户
     *
     * @param domainId 系统组织账户主键
     * @return 系统组织账户
     */
    @Override
    public SysDomain selectSysDomainByDomainId(String domainId)
    {
        return sysDomainMapper.selectSysDomainByDomainId(domainId);
    }

    /**
     * 查询系统组织账户列表
     *
     * @param sysDomain 系统组织账户
     * @return 系统组织账户
     */
    @Override
    public List<SysDomain> selectSysDomainList(SysDomain sysDomain)
    {
        return sysDomainMapper.selectSysDomainList(sysDomain);
    }


    /**
     * 修改系统组织账户
     *
     * @param sysDomain 系统组织账户
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateSysDomain(SysDomain sysDomain)
    {
        sysDomain.setUpdateBy(SecurityUtils.getLoginUser().getUser().getUserName());
        sysDomain.setUpdateTime(DateUtils.getNowDate());
        return sysDomainMapper.updateSysDomain(sysDomain);
    }

    /**
     * 创建企业组织账户信息
     *
     * @param dcCompBasic 企业基础信息
     * @return SysDomain 返回企业组织账户
     */
    @Override
    public SysDomain createCompDomainAccount(DcCompBasic dcCompBasic)
    {
        SysDomain sysDomain = new SysDomain();
        sysDomain.setDomainId(IdWorker.nextId().toString());
        sysDomain.setDomainParentId(SysConstants.DOMAIN_COMP); //企业组织域
        sysDomain.setDrolesId(SysConstants.DOMAIN_ROLE_COMP);  //企业组织角色
        sysDomain.setDomainName(dcCompBasic.getCompName());
        sysDomain.setDomainAccount(SysConstants.PREFIX_COMP_ACCOUNT + dcCompBasic.getCompCreditCode()); //企业管理用户账号
        Calendar rightNowDate = Calendar.getInstance();
        rightNowDate.add(Calendar.YEAR, 20); //有效期20年
        sysDomain.setDomainIndate(rightNowDate.getTime()); //组织账户有效期
        sysDomain.setDomainRegion(dcCompBasic.getCompArea());
        sysDomain.setDomainPhone(dcCompBasic.getCompLxrPhone());
        sysDomain.setDomainEmail(dcCompBasic.getCompLxrEmail());
        sysDomain.setDomainRelationId(dcCompBasic.getCompBasicId()); //组织账号与业务关系ID
        sysDomain.setCreateBy(dcCompBasic.getCreateBy());
        sysDomain.setCreateTime(dcCompBasic.getCreateTime());
        sysDomainMapper.insertSysDomain(sysDomain);
        return sysDomain;
    }

}
