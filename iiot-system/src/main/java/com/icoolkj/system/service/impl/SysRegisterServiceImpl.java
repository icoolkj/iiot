package com.icoolkj.system.service.impl;

import com.icoolkj.common.core.domain.entity.SysRegister;
import com.icoolkj.common.utils.DateUtils;
import com.icoolkj.common.utils.uuid.IdWorker;
import com.icoolkj.system.mapper.SysRegisterMapper;
import com.icoolkj.system.service.ISysRegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 注册信息Service业务层处理
 *
 * @author icoolkj
 */
@Service
public class SysRegisterServiceImpl implements ISysRegisterService
{
    @Autowired
    private SysRegisterMapper sysRegisterMapper;

    /**
     * 查询注册信息
     *
     * @param registerId 注册信息主键
     * @return 注册信息
     */
    @Override
    public SysRegister selectSysRegisterByRegisterId(String registerId)
    {
        return sysRegisterMapper.selectSysRegisterByRegisterId(registerId);
    }

    /**
     * 查询注册信息列表
     *
     * @param sysRegister 注册信息
     * @return 注册信息
     */
    @Override
    public List<SysRegister> selectSysRegisterList(SysRegister sysRegister)
    {
        return sysRegisterMapper.selectSysRegisterList(sysRegister);
    }

    /**
     * 新增注册信息
     *
     * @param sysRegister 注册信息
     * @return 结果
     */
    @Override
    public int insertSysRegister(SysRegister sysRegister)
    {
        sysRegister.setRegisterId(IdWorker.nextId().toString());
        sysRegister.setCreateTime(DateUtils.getNowDate());
        return sysRegisterMapper.insertSysRegister(sysRegister);
    }

    /**
     * 修改注册信息
     *
     * @param sysRegister 注册信息
     * @return 结果
     */
    @Override
    public int updateSysRegister(SysRegister sysRegister)
    {
        return sysRegisterMapper.updateSysRegister(sysRegister);
    }

    @Override
    public SysRegister checkRegisterAccountUnique(String registerType, String registerAccount, String registerNickName)
    {
        return sysRegisterMapper.checkRegisterAccountUnique(registerType, registerAccount, registerNickName );
    }

}
