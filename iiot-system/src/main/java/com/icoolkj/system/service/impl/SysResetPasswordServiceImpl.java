package com.icoolkj.system.service.impl;

import com.icoolkj.common.core.domain.entity.SysResetPassword;
import com.icoolkj.common.utils.DateUtils;
import com.icoolkj.common.utils.uuid.IdWorker;
import com.icoolkj.system.mapper.SysResetPasswordMapper;
import com.icoolkj.system.service.ISysResetPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 密码重置信息Service业务层处理
 *
 * @author icoolkj
 */
@Service
public class SysResetPasswordServiceImpl implements ISysResetPasswordService
{
    @Autowired
    private SysResetPasswordMapper sysResetPasswordMapper;

    /**
     * 查询密码重置信息
     *
     * @param resetPwdId 密码重置信息主键
     * @return 密码重置信息
     */
    @Override
    public SysResetPassword selectSysResetPasswordByResetPwdId(String resetPwdId)
    {
        return sysResetPasswordMapper.selectSysResetPasswordByResetPwdId(resetPwdId);
    }

    /**
     * 查询密码重置信息列表
     *
     * @param sysResetPassword 密码重置信息
     * @return 密码重置信息
     */
    @Override
    public List<SysResetPassword> selectSysResetPasswordList(SysResetPassword sysResetPassword)
    {
        return sysResetPasswordMapper.selectSysResetPasswordList(sysResetPassword);
    }

    /**
     * 新增密码重置信息
     *
     * @param sysResetPassword 密码重置信息
     * @return 结果
     */
    @Override
    public int insertSysResetPassword(SysResetPassword sysResetPassword)
    {
        sysResetPassword.setResetPwdId(IdWorker.nextId().toString());
        sysResetPassword.setCreateTime(DateUtils.getNowDate());
        return sysResetPasswordMapper.insertSysResetPassword(sysResetPassword);
    }

    /**
     * 修改密码重置信息
     *
     * @param sysResetPassword 密码重置信息
     * @return 结果
     */
    @Override
    public int updateSysResetPassword(SysResetPassword sysResetPassword)
    {
        return sysResetPasswordMapper.updateSysResetPassword(sysResetPassword);
    }

    /**
     * 批量删除密码重置信息
     *
     * @param resetPwdIds 需要删除的密码重置信息主键
     * @return 结果
     */
    @Override
    public int deleteSysResetPasswordByResetPwdIds(String[] resetPwdIds)
    {
        return sysResetPasswordMapper.deleteSysResetPasswordByResetPwdIds(resetPwdIds);
    }

    /**
     * 删除密码重置信息信息
     *
     * @param resetPwdId 密码重置信息主键
     * @return 结果
     */
    @Override
    public int deleteSysResetPasswordByResetPwdId(String resetPwdId)
    {
        return sysResetPasswordMapper.deleteSysResetPasswordByResetPwdId(resetPwdId);
    }
}
