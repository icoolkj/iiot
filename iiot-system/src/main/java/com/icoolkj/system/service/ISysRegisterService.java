package com.icoolkj.system.service;

import com.icoolkj.common.core.domain.entity.SysRegister;

import java.util.List;

/**
 * 注册信息Service接口
 *
 * @author icoolkj
 */
public interface ISysRegisterService
{
    /**
     * 查询注册信息
     *
     * @param registerId 注册信息主键
     * @return 注册信息
     */
    public SysRegister selectSysRegisterByRegisterId(String registerId);

    /**
     * 查询注册信息列表
     *
     * @param sysRegister 注册信息
     * @return 注册信息集合
     */
    public List<SysRegister> selectSysRegisterList(SysRegister sysRegister);

    /**
     * 新增注册信息
     *
     * @param sysRegister 注册信息
     * @return 结果
     */
    public int insertSysRegister(SysRegister sysRegister);

    /**
     * 修改注册信息
     *
     * @param sysRegister 注册信息
     * @return 结果
     */
    public int updateSysRegister(SysRegister sysRegister);

    public SysRegister checkRegisterAccountUnique(String registerType, String registerAccount, String registerNickName);
}
