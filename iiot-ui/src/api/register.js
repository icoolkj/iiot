import request from '@/utils/request'

// 企业注册方法
export function compRegister(data) {
  return request({
    url: '/compRegister',
    headers: {
      isToken: false
    },
    method: 'post',
    data: data
  })
}
