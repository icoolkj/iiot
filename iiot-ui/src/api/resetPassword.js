import request from '@/utils/request'

export function getUserInfo(data) {
  return request({
    url: '/resetPassword/getUserInfo',
    headers: {
      isToken: false
    },
    method: 'post',
    data: data
  })
}
