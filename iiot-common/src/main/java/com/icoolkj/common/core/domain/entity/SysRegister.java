package com.icoolkj.common.core.domain.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.io.Serializable;
import java.util.Date;

/**
 * 注册信息对象 sys_register
 *
 * @author icoolkj
 */
public class SysRegister implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String registerId;

    /** 注册类型【企业COMP,服务商FWS,综合服务机构ZHFWJG】 */
    private String registerType;

    /** 账号【企业统一社会信用代码】 */
    private String registerAccount;

    /** 昵称【企业名称】 */
    private String registerNickName;

    /** 注册联系人 */
    private String registerLxr;

    /** 注册联系人手机 */
    private String registerLxrPhone;

    /** 注册联系人邮箱 */
    private String registerLxrEmail;

    /** 注册时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date registerDate;

    /** 密码 */
    private String registerPassword;

    /** 账号注册状态（1代表申请中|2代表审核通过|3代表审核不通过） */
    private String registerStatus;

    /** 删除标记（0代表存在|2代表删除） */
    private String delFlag;

    /** 审核人 */
    private String auditBy;

    /** 审核时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date auditTime;

    /** 审核意见 */
    private String auditIdea;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    public void setRegisterId(String registerId)
    {
        this.registerId = registerId;
    }

    public String getRegisterId()
    {
        return registerId;
    }
    public void setRegisterType(String registerType)
    {
        this.registerType = registerType;
    }

    public String getRegisterType()
    {
        return registerType;
    }
    public void setRegisterAccount(String registerAccount)
    {
        this.registerAccount = registerAccount;
    }

    public String getRegisterAccount()
    {
        return registerAccount;
    }
    public void setRegisterNickName(String registerNickName)
    {
        this.registerNickName = registerNickName;
    }

    public String getRegisterNickName()
    {
        return registerNickName;
    }
    public void setRegisterLxr(String registerLxr)
    {
        this.registerLxr = registerLxr;
    }

    public String getRegisterLxr()
    {
        return registerLxr;
    }
    public void setRegisterLxrPhone(String registerLxrPhone)
    {
        this.registerLxrPhone = registerLxrPhone;
    }

    public String getRegisterLxrPhone()
    {
        return registerLxrPhone;
    }
    public void setRegisterLxrEmail(String registerLxrEmail)
    {
        this.registerLxrEmail = registerLxrEmail;
    }

    public String getRegisterLxrEmail()
    {
        return registerLxrEmail;
    }
    public void setRegisterDate(Date registerDate)
    {
        this.registerDate = registerDate;
    }

    public Date getRegisterDate()
    {
        return registerDate;
    }
    public void setRegisterPassword(String registerPassword)
    {
        this.registerPassword = registerPassword;
    }

    public String getRegisterPassword()
    {
        return registerPassword;
    }
    public void setRegisterStatus(String registerStatus)
    {
        this.registerStatus = registerStatus;
    }

    public String getRegisterStatus()
    {
        return registerStatus;
    }
    public void setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag()
    {
        return delFlag;
    }
    public void setAuditBy(String auditBy)
    {
        this.auditBy = auditBy;
    }

    public String getAuditBy()
    {
        return auditBy;
    }
    public void setAuditTime(Date auditTime)
    {
        this.auditTime = auditTime;
    }

    public Date getAuditTime()
    {
        return auditTime;
    }
    public void setAuditIdea(String auditIdea)
    {
        this.auditIdea = auditIdea;
    }

    public String getAuditIdea()
    {
        return auditIdea;
    }
    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }
    public Date getCreateTime()
    {
        return createTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("registerId", getRegisterId())
            .append("registerType", getRegisterType())
            .append("registerAccount", getRegisterAccount())
            .append("registerNickName", getRegisterNickName())
            .append("registerLxr", getRegisterLxr())
            .append("registerLxrPhone", getRegisterLxrPhone())
            .append("registerLxrEmail", getRegisterLxrEmail())
            .append("registerDate", getRegisterDate())
            .append("registerPassword", getRegisterPassword())
            .append("registerStatus", getRegisterStatus())
            .append("delFlag", getDelFlag())
            .append("createTime", getCreateTime())
            .append("auditBy", getAuditBy())
            .append("auditTime", getAuditTime())
            .append("auditIdea", getAuditIdea())
            .toString();
    }
}
