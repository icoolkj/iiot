package com.icoolkj.common.core.domain.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

/**
 * 用户注册对象
 *
 * @author icoolkj
 */
public class CompRegisterBody extends RegisterBody
{
    private static final long serialVersionUID = 1L;

    /** 注册类型【企业COMP,服务商FWS,综合服务机构ZHFWJG】 */
    @NotBlank(message = "注册类型不能为空")
    @Size(min = 0, max = 10, message = "注册类型长度不能超过10个字符")
    private String registerType;

    /** 账号【企业统一社会信用代码】 */
    @NotBlank(message = "统一社会信用代码不能为空")
    @Size(min = 18, max = 18, message = "统一社会信用代码长度为18个字符")
    private String registerAccount;

    /** 昵称【企业名称】 */
    @NotBlank(message = "注册企业名称不能为空")
    @Size(min = 2, max = 50, message = "注册企业名称长度不能超过50个字符")
    private String registerNickName;

    /** 注册联系人 */
    @NotBlank(message = "注册联系人不能为空")
    @Size(min = 0, max = 20, message = "注册联系人长度不能超过20个字符")
    private String registerLxr;

    /** 注册联系人手机 */
    @NotBlank(message = "注册联系人手机不能为空")
    @Size(min = 11, max = 11, message = "联系人手机长度为11个字符")
    private String registerLxrPhone;

    /** 注册联系人邮箱 */
    @NotBlank(message = "注册联系人邮箱不能为空")
    private String registerLxrEmail;

    /** 密码 */
    private String registerPassword;

    public String getRegisterType() {
        return registerType;
    }

    public void setRegisterType(String registerType) {
        this.registerType = registerType;
    }

    public String getRegisterAccount() {
        return registerAccount;
    }

    public void setRegisterAccount(String registerAccount) {
        this.registerAccount = registerAccount;
    }

    public String getRegisterNickName() {
        return registerNickName;
    }

    public void setRegisterNickName(String registerNickName) {
        this.registerNickName = registerNickName;
    }

    public String getRegisterLxr() {
        return registerLxr;
    }

    public void setRegisterLxr(String registerLxr) {
        this.registerLxr = registerLxr;
    }

    public String getRegisterLxrPhone() {
        return registerLxrPhone;
    }

    public void setRegisterLxrPhone(String registerLxrPhone) {
        this.registerLxrPhone = registerLxrPhone;
    }

    public String getRegisterLxrEmail() {
        return registerLxrEmail;
    }

    public void setRegisterLxrEmail(String registerLxrEmail) {
        this.registerLxrEmail = registerLxrEmail;
    }

    public String getRegisterPassword() {
        return registerPassword;
    }

    public void setRegisterPassword(String registerPassword) {
        this.registerPassword = registerPassword;
    }

    @Override
    public String toString() {
        return "CompRegisterBody{" +
                "registerType='" + registerType + '\'' +
                ", registerAccount='" + registerAccount + '\'' +
                ", registerNickName='" + registerNickName + '\'' +
                ", registerLxr='" + registerLxr + '\'' +
                ", registerLxrPhone='" + registerLxrPhone + '\'' +
                ", registerLxrEmail='" + registerLxrEmail + '\'' +
                ", registerPassword='" + registerPassword + '\'' +
                '}';
    }
}
