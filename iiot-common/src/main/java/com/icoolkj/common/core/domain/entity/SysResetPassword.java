package com.icoolkj.common.core.domain.entity;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.icoolkj.common.annotation.Excel;
import com.icoolkj.common.core.domain.BaseEntity;

/**
 * 密码找回（重置）信息对象 sys_reset_password
 *
 * @author icoolkj
 * @date 2024-01-05
 */
public class SysResetPassword extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    private String resetPwdId;

    /** 验证方式（1代表电子邮箱|2代表手机验证） */
    @Excel(name = "验证方式", readConverterExp = "1=代表电子邮箱|2代表手机验证")
    private String verifyMode;

    /** 用户类型 */
    @Excel(name = "用户类型")
    private String userType;

    /** 用户ID */
    @Excel(name = "用户ID")
    private String userId;

    /** 用户账号 */
    @Excel(name = "用户账号")
    private String userName;

    /** 手机号码 */
    @Excel(name = "手机号码")
    private String userPhone;

    /** 用户邮箱 */
    @Excel(name = "用户邮箱")
    private String userEmail;

    /** 重置密码的唯一标识符，使用随机生成的字符串 */
    @Excel(name = "重置密码的唯一标识符，使用随机生成的字符串")
    private String token;

    /** 验证码 */
    @Excel(name = "验证码")
    private String verifyCode;

    /** 重置密码链接的有效时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "重置密码链接的有效时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date expiryTime;

    /** 状态（0代表正常|1代表已失效|2代表完成） */
    @Excel(name = "状态", readConverterExp = "0=代表正常|1代表已失效|2代表完成")
    private String expiryFlag;

    /** 完成时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "完成时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date finishTime;

    public void setResetPwdId(String resetPwdId)
    {
        this.resetPwdId = resetPwdId;
    }

    public String getResetPwdId()
    {
        return resetPwdId;
    }
    public void setVerifyMode(String verifyMode)
    {
        this.verifyMode = verifyMode;
    }

    public String getVerifyMode()
    {
        return verifyMode;
    }
    public void setUserType(String userType)
    {
        this.userType = userType;
    }

    public String getUserType()
    {
        return userType;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserId()
    {
        return userId;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public String getUserName()
    {
        return userName;
    }
    public void setUserPhone(String userPhone)
    {
        this.userPhone = userPhone;
    }

    public String getUserPhone()
    {
        return userPhone;
    }
    public void setUserEmail(String userEmail)
    {
        this.userEmail = userEmail;
    }

    public String getUserEmail()
    {
        return userEmail;
    }
    public void setToken(String token)
    {
        this.token = token;
    }

    public String getToken()
    {
        return token;
    }
    public void setVerifyCode(String verifyCode)
    {
        this.verifyCode = verifyCode;
    }

    public String getVerifyCode()
    {
        return verifyCode;
    }
    public void setExpiryTime(Date expiryTime)
    {
        this.expiryTime = expiryTime;
    }

    public Date getExpiryTime()
    {
        return expiryTime;
    }
    public void setExpiryFlag(String expiryFlag)
    {
        this.expiryFlag = expiryFlag;
    }

    public String getExpiryFlag()
    {
        return expiryFlag;
    }
    public void setFinishTime(Date finishTime)
    {
        this.finishTime = finishTime;
    }

    public Date getFinishTime()
    {
        return finishTime;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("resetPwdId", getResetPwdId())
            .append("verifyMode", getVerifyMode())
            .append("userType", getUserType())
            .append("userId", getUserId())
            .append("userName", getUserName())
            .append("userPhone", getUserPhone())
            .append("userEmail", getUserEmail())
            .append("token", getToken())
            .append("verifyCode", getVerifyCode())
            .append("expiryTime", getExpiryTime())
            .append("expiryFlag", getExpiryFlag())
            .append("createTime", getCreateTime())
            .append("finishTime", getFinishTime())
            .toString();
    }
}
