package com.icoolkj.common.core.domain.model;

/**
 * 用户注册对象
 *
 * @author icoolkj
 */
public class RegisterBody
{
    private static final long serialVersionUID = 1L;

    /** 验证码 */
    private String code;

    /** 唯一标识 */
    private String uuid;

    public String getCode() { return code; }

    public void setCode(String code) { this.code = code; }

    public String getUuid() { return uuid; }

    public void setUuid(String uuid) { this.uuid = uuid; }
}
