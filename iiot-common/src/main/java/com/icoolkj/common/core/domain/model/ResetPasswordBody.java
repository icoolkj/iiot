package com.icoolkj.common.core.domain.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ResetPasswordBody
{
    private static final long serialVersionUID = 1L;

    @NotBlank(message = "用户类型")
    @Size(min = 0, max = 10, message = "用户类型长度不能超过10个字符")
    private String userType;

    /** 账号【企业统一社会信用代码】 */
    @NotBlank(message = "用户账号不能为空")
    @Size(min = 0, max = 30, message = "用户账号长度不能超过30个字符")
    private String userName;

    /** 验证码 */
    private String code;

    /** 唯一标识 */
    private String uuid;


    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCode() { return code; }

    public void setCode(String code) { this.code = code; }

    public String getUuid() { return uuid; }

    public void setUuid(String uuid) { this.uuid = uuid; }
}
