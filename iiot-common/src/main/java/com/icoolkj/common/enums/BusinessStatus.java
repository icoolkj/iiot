package com.icoolkj.common.enums;

/**
 * 操作状态
 * 
 * @author icoolkj
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
