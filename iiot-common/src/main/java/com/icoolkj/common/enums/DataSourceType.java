package com.icoolkj.common.enums;

/**
 * 数据源
 * 
 * @author icoolkj
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
